package main.java.controller;


import main.java.UsersClasses.ExcelExport;
import main.java.UsersClasses.FileModel;
import main.java.UsersClasses.FrameGrabberModel;
import main.java.UsersClasses.HeartBeatSearch;
import main.java.UsersClasses.Chart;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by alex on 01.04.16.
 */
public class Controller implements Runnable{
    public static ArrayList<Float> results;

    public static Thread getThread() {
        return thread;
    }

    public static Thread thread;
    public Controller(){
        thread = new Thread(this, "FrameGrabberModel");
        thread.start();
    }

    public static boolean runMotionDetector()  {
        results = new ArrayList<>();
        try{
            Controller controller = new Controller();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean loadVideo()  {
        try{
            FileModel.loadVideo();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean exportToExcell()  {
        HeartBeatSearch heartBeatSearch = new HeartBeatSearch();
        try{
            if(FrameGrabberModel.status == false) {
                //results = heartBeatSearch.getAverageValue(results);
                ExcelExport.exportResultsToExcellFile(results);
            }else {
                JOptionPane.showMessageDialog(null, "Wait until the end of the video processing");
            }
        }catch(NullPointerException e){
            JOptionPane.showMessageDialog(null, "No results for export");
            return false;
        }
        return true;
    }


    @Override
    public void run() {
        FrameGrabberModel frameGrabberModel = new FrameGrabberModel();
        try {
            results = frameGrabberModel.motionDetection(FileModel.getVideoPath());
            HeartBeatSearch heartBeatSearch= new HeartBeatSearch();
            results = heartBeatSearch.getAverageValue(results);
            FrameGrabberModel.status = false;

            double x[] = new double[results.size()];
            double y[] = new double[results.size()];
            for (int i = 0; i < x.length; i++) {
                x[i] = i + 1;
                y[i] = results.get(i);
            }
            Chart.displayChart(x, y);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Wrong way to video");
            FrameGrabberModel.status = false;
        }

    }
}
