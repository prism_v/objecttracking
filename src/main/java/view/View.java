package main.java.view;

import javax.swing.border.*;

import main.java.UsersClasses.FrameGrabberModel;
import main.java.controller.Controller;
import org.bytedeco.javacpp.presets.opencv_core;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;
/*
 * Created by JFormDesigner on Fri Apr 01 23:08:47 EEST 2016
 */


/**
 * @author Brainrain
 */
public class View extends JFrame {
	public static String path = "main_picture.png";
	public View() {
		initComponents();
	}

	private void RunObjectFinder(ActionEvent e) {
		// TODO add your code here
	}

	private void btn_start_ActionListener(ActionEvent e) {
		// TODO add your code here
		if(FrameGrabberModel.status == false)
			Controller.runMotionDetector();
		else
			JOptionPane.showMessageDialog(null, "In Progress!");
	}

	private void loadVideoActionPerformed(ActionEvent e) {
		// TODO add your code here
		Controller.loadVideo();
	}

	private void ExportToExcellActionPerformed(ActionEvent e) {
		// TODO add your code here
		Controller.exportToExcell();
	}

	private void LoadVideoActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void ExportResultsActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void ExitActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		menuBar1 = new JMenuBar();
		menu1 = new JMenu();
		menuItem2 = new JMenuItem();
		menuItem7 = new JMenuItem();
		Exit = new JMenuItem();
		menu2 = new JMenu();
		menuItem5 = new JMenuItem();
		menuItem6 = new JMenuItem();
		btn_loadVideo = new JButton();
		progressBar1 = new JProgressBar();
		lbl_videoFrame = new JLabel();
		loadVideoPath = new JTextField();
		button1 = new JButton();
		panel1 = new JPanel();
		button2 = new JButton();
		button4 = new JButton();
		panel2 = new JPanel();
		lbl_FrameRate = new JLabel();
		lbl_Resolution = new JLabel();
		lbl_Bitrate = new JLabel();
		lbl_Processed = new JLabel();
		lbl_line = new JLabel();
		lbl_FrameRateAnwer = new JLabel();
		lbl_ResolutionAnwer = new JLabel();
		lbl_BitrateAnswer = new JLabel();
		lbl_ProcessedAnswer = new JLabel();

		//======== this ========
		setResizable(false);
		setFocusable(false);
		setTitle("VideoAnalyzer v1.0");
		Container contentPane = getContentPane();
		/*File file = new File("images/main_picture.png");
		if(file.exists()){
		JOptionPane.showMessageDialog(null, "есть картинка");
		}else {
			JOptionPane.showMessageDialog(null, "нет картинки");
		}*/
		//ImageIcon icon3 = new ImageIcon("images/main_picture.png");
		URL imgURL = View.class.getResource(path);
		ImageIcon icon3 = new ImageIcon(imgURL);
		ImageIcon icon4 = new ImageIcon(icon3.getImage().getScaledInstance(344, 289, Image.SCALE_SMOOTH));
		lbl_videoFrame.setIcon(icon4);

		//======== menuBar1 ========
		{

			//======== menu1 ========
			{
				menu1.setText("File");

				//---- menuItem2 ----
				menuItem2.setText("Load video");
				menuItem2.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						LoadVideoActionPerformed(e);
						Controller.loadVideo();
					}
				});
				menu1.add(menuItem2);

				//---- menuItem7 ----
				menuItem7.setText("Export results");
				menuItem7.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ExportResultsActionPerformed(e);
						Controller.exportToExcell();
					}
				});
				menu1.add(menuItem7);

				//---- Exit ----
				Exit.setText("Exit");
				Exit.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ExitActionPerformed(e);
						System.exit(0);
					}
				});
				menu1.add(Exit);
			}
			menuBar1.add(menu1);

			//======== menu2 ========
			{
				menu2.setText("Help");
				menu2.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						RunObjectFinder(e);
					}
				});

				//---- menuItem5 ----
//				menuItem5.setText("Help Content");
//				menu2.add(menuItem5);

				//---- menuItem6 ----
				menuItem6.setText("About");
				menuItem6.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						RunObjectFinder(e);
						JOptionPane.showMessageDialog(null, "Program name: Video Analyzer \n Current Version: 1.0");
					}

				});
				menu2.add(menuItem6);
			}
			menuBar1.add(menu2);
		}
		setJMenuBar(menuBar1);

		//---- btn_loadVideo ----
		btn_loadVideo.setText("Load Video");
		btn_loadVideo.setPreferredSize(new Dimension(80, 20));
		btn_loadVideo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadVideoActionPerformed(e);
			}
		});

		//---- lbl_videoFrame ----
		lbl_videoFrame.setAlignmentY(0.0F);

		//---- loadVideoPath ----
		loadVideoPath.setEditable(false);
		loadVideoPath.setBorder(new CompoundBorder(
			new BevelBorder(BevelBorder.LOWERED),
			null));
		loadVideoPath.setText("File path...");

		//---- button1 ----
		button1.setText("Export to Excell");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ExportToExcellActionPerformed(e);
			}
		});

		//======== panel1 ========
		{
			panel1.setPreferredSize(new Dimension(344, 60));

			//---- button2 ----
			button2.setText("Start");
			button2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btn_start_ActionListener(e);
				}
			});

			//---- button4 ----
			button4.setText("Stop");
			button4.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(FrameGrabberModel.status == true) {
						FrameGrabberModel.status = false;
						Controller.getThread().stop();
					}

				}
			});
			GroupLayout panel1Layout = new GroupLayout(panel1);
			panel1.setLayout(panel1Layout);
			panel1Layout.setHorizontalGroup(
				panel1Layout.createParallelGroup()
					.addComponent(button2, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)
					.addComponent(button4, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)
			);
			panel1Layout.setVerticalGroup(
				panel1Layout.createParallelGroup()
					.addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
						.addContainerGap(19, Short.MAX_VALUE)
						.addComponent(button2, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(button4))
			);
		}

		//======== panel2 ========
		{
			panel2.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));

			//---- lbl_FrameRate ----
			lbl_FrameRate.setText("Frame rate");

			//---- lbl_Resolution ----
			lbl_Resolution.setText("Resolution");

			//---- lbl_Bitrate ----
			lbl_Bitrate.setText("Bitrate");

			//---- lbl_Processed ----
			lbl_Processed.setText("Processed");

			//---- lbl_line ----
			lbl_line.setText("____________________________________________");

			//---- lbl_FrameRateAnwer ----
			lbl_FrameRateAnwer.setText("NaN");

			//---- lbl_ResolutionAnwer ----
			lbl_ResolutionAnwer.setText("NaN");

			//---- lbl_BitrateAnswer ----
			lbl_BitrateAnswer.setText("NaN");

			//---- lbl_ProcessedAnswer ----
			lbl_ProcessedAnswer.setText("NaN");

			GroupLayout panel2Layout = new GroupLayout(panel2);
			panel2.setLayout(panel2Layout);
			panel2Layout.setHorizontalGroup(
				panel2Layout.createParallelGroup()
					.addGroup(panel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(panel2Layout.createParallelGroup()
							.addGroup(panel2Layout.createSequentialGroup()
								.addComponent(lbl_Processed, GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(lbl_ProcessedAnswer, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
								.addGap(35, 35, 35))
							.addComponent(lbl_line, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
							.addGroup(panel2Layout.createSequentialGroup()
								.addComponent(lbl_Bitrate, GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
								.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(lbl_BitrateAnswer, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
								.addGap(40, 40, 40))
							.addGroup(panel2Layout.createSequentialGroup()
								.addGroup(panel2Layout.createParallelGroup()
									.addComponent(lbl_FrameRate, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
									.addComponent(lbl_Resolution, GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
								.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(panel2Layout.createParallelGroup()
									.addComponent(lbl_FrameRateAnwer)
									.addComponent(lbl_ResolutionAnwer, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
								.addContainerGap())))
			);
			panel2Layout.setVerticalGroup(
				panel2Layout.createParallelGroup()
					.addGroup(panel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(panel2Layout.createParallelGroup()
							.addComponent(lbl_FrameRate)
							.addComponent(lbl_FrameRateAnwer))
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
						.addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
							.addComponent(lbl_Resolution)
							.addComponent(lbl_ResolutionAnwer))
						.addGap(28, 28, 28)
						.addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
							.addComponent(lbl_Bitrate, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addComponent(lbl_BitrateAnswer))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(lbl_line)
						.addGap(18, 18, 18)
						.addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
							.addComponent(lbl_Processed, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addComponent(lbl_ProcessedAnswer, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
						.addGap(22, 22, 22))
			);
		}

		GroupLayout contentPaneLayout = new GroupLayout(contentPane);
		contentPane.setLayout(contentPaneLayout);
		contentPaneLayout.setHorizontalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(contentPaneLayout.createParallelGroup()
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addComponent(btn_loadVideo, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
							.addGap(18, 18, 18)
							.addComponent(loadVideoPath, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE))
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
								.addComponent(progressBar1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lbl_videoFrame, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
							.addGroup(contentPaneLayout.createParallelGroup()
								.addComponent(panel2, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
								.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
									.addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)
									.addComponent(button1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)))))
					.addGap(11, 11, 11))
		);
		contentPaneLayout.setVerticalGroup(
			contentPaneLayout.createParallelGroup()
				.addGroup(contentPaneLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(btn_loadVideo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(loadVideoPath, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addGroup(GroupLayout.Alignment.LEADING, contentPaneLayout.createSequentialGroup()
							.addComponent(lbl_videoFrame, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
							.addComponent(progressBar1, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
						.addGroup(contentPaneLayout.createSequentialGroup()
							.addComponent(button1)
							.addGap(18, 18, 18)
							.addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
							.addComponent(panel1, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(13, Short.MAX_VALUE))
		);
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JMenuBar menuBar1;
	private JMenu menu1;
	private JMenuItem menuItem2;
	private JMenuItem menuItem7;
	private JMenuItem Exit;
	private JMenu menu2;
	private JMenuItem menuItem5;
	private JMenuItem menuItem6;
	private JButton btn_loadVideo;
	private static JProgressBar progressBar1;
	private static JLabel lbl_videoFrame;
	private static JTextField loadVideoPath;
	private JButton button1;
	private JPanel panel1;
	private JButton button2;
	private JButton button4;
	private JPanel panel2;
	private JLabel lbl_FrameRate;
	private JLabel lbl_Resolution;
	private JLabel lbl_Bitrate;
	private JLabel lbl_Processed;
	private JLabel lbl_line;
	public static JLabel lbl_FrameRateAnwer;
	public static JLabel lbl_ResolutionAnwer;
	public static JLabel lbl_BitrateAnswer;
	public static JLabel lbl_ProcessedAnswer;
// JFormDesigner - End of variables declaration  //GEN-END:variables

	public static JLabel getLbl_FrameRateAnwer() {
		return lbl_FrameRateAnwer;
	}

	public JLabel getLbl_ResolutionAnwer() {
		return lbl_ResolutionAnwer;
	}

	public JLabel getLbl_BitreteAnswer() {
		return lbl_BitrateAnswer;
	}

	public JLabel getLbl_ProcessedAnswer() {
		return lbl_ProcessedAnswer;
	}

	public JLabel getLbl_FrameRate() {
		return lbl_FrameRate;
	}

	public static JLabel getLabel1() {
		return lbl_videoFrame;
	}

	public static JTextField getLoadVideoPath() {
		return loadVideoPath;
	}

	public static JProgressBar getJProgressBar() {
		return progressBar1;
	}





}
