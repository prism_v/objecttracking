package main.java.UsersClasses;

import main.java.view.View;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by alex on 24.04.16.
 */
public class FileModel {

    static File videoFile = null;

    public static void loadVideo(){
        FileFilter filter = new FileNameExtensionFilter("mp4 video format", "mp4");
        JFileChooser fileopen = new JFileChooser();
        fileopen.setFileFilter(filter);
        int ret = fileopen.showDialog(null, "Open file");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileopen.getSelectedFile();
            videoFile = file;
            View.getLoadVideoPath().setText(file.getPath());
        }
    }

    public static File getVideoPath(){
        return videoFile;
    }

    public static void saveResults(){
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.xls","*.*");
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(filter);
        if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ) {
            try ( FileWriter fw = new FileWriter(fc.getSelectedFile()) ) {
                fw.write("Blah blah blah...");
            }
            catch ( IOException e ) {
                System.out.println("Всё погибло!");
            }
        }
    }
}
