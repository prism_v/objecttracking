package main.java.UsersClasses;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by alex on 23.04.16.
 */
public class ExcelExport {
    public static void exportResultsToExcellFile(ArrayList<Float> listSizePoint) {
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet("Sheet1");

        Row row = sheet.createRow(0);
        Cell count = row.createCell(0);
        count.setCellValue("Date of experiment " + new Date(System.currentTimeMillis()).toString());

        int shift = 1;

        for (int i = 0; i < listSizePoint.size(); i++) {
            row = sheet.createRow(i + shift);
            row.createCell(0).setCellValue(listSizePoint.get(i));
        }

//        // Записываем всё в файл
//        try {
//            book.write(new FileOutputStream(fileName + ".xls"));
//            book.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.xls", "*.*");
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(filter);
        if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            try{
                book.write(new FileOutputStream(fc.getSelectedFile() + ".xls"));
                book.close();
            } catch (IOException e) {
                System.out.println("Всё погибло!");
            }
        }

    }
}
