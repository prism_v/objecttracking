package main.java.UsersClasses;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

public class Chart {

    public static void displayChart(double[] xData, double[] yData){
        // Create Chart
        XYChart chart = QuickChart.getChart("Breathing rate", "t,sec", "N", "N(t)", xData, yData);

        // Show it
        new SwingWrapper(chart).displayChart();
    }
}
