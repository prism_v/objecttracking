package main.java.UsersClasses;

import java.util.ArrayList;

/**
 * Created by    alex on 08.06.16.
 */
public class HeartBeatSearch {

    String condition = "UP";
    String prevCondition;
    float prevConditionCounter = 0;
    float changeConditionCounter = 1;
        public ArrayList<Float> getAverageValue(ArrayList<Float> arrayPointSize){
            ArrayList<Float> results = new ArrayList<>();
            float tempValue = 0;
            float prevTempValue = 0;
            for (int i = 0; i < arrayPointSize.size(); i++) {
                if(arrayPointSize.size() > i + 1) {
                    tempValue = arrayPointSize.get(i + 1);
                }

                prevTempValue = arrayPointSize.get(i);

                prevCondition = condition;

                switch (condition){
                    case "UP":{
                        if(prevTempValue < tempValue) {
                            condition = "DOWN";
                        }
                        break;
                    }
                    case "DOWN":{
                        if(prevTempValue > tempValue){
                            condition = "UP";
                        }
                        break;
                    }
                }

/*                //set counter to 0 if new minute start
                if(i > 0 && i % 30 == 0){
                    changeConditionCounter = 1;
                }*/
                if(prevCondition != condition){
                    changeConditionCounter++;
                }

                if(i > 0 && i % 30 == 0){
                    int currentSecond = i / 30;
                    System.out.println(currentSecond + " Количество сокращений / минута = " + ((changeConditionCounter - prevConditionCounter) / 4));

                    results.add((changeConditionCounter - prevConditionCounter) / 4);

                    prevConditionCounter = changeConditionCounter;
                }



            }

            return results;
        }
}
