package main.java.UsersClasses;

import main.java.Start;
import main.java.controller.Controller;
import main.java.view.View;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacv.*;
import org.bytedeco.javacv.Frame;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import static org.bytedeco.javacpp.helper.opencv_imgproc.cvFindContours;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class FrameGrabberModel{
    static JProgressBar jProgressBar;
    static JLabel jLabel;
    public static boolean status = false;


    public ArrayList motionDetection(File file) throws Exception {
        //        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        //FFmpegFrameGrabber grabber = new FFmpegFrameGrabber("video/Daphnia720p.mp4");
        //FFmpegFrameGrabber grabber = new FFmpegFrameGrabber("video/1Daphnia720p_xvid.mp4");
//        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber("/media/ghost/7066CF8F28E9FD5B/work/Kalushnuy/Diplom/111.avi");
        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
//        grabber.setFormat("avi");
        //File file = new File(pathToVideo);
        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file);
        //FFmpegFrameGrabber grabber = new FFmpegFrameGrabber("video/Daphnia720p.mp4");
        grabber.setFormat("mp4");

        check2("до старта грабера");
        check(file);
        grabber.start();
        check2("после старта грабера");

        //=============================setVideoInfo===============================
        //Start.view.getLbl_BitreteAnswer().setText(Double.toString(grabber.getVideoBitrate()));
        Start.view.getLbl_ResolutionAnwer().setText(Integer.toString(grabber.getImageWidth()) + "x" + grabber.getImageHeight());
        Start.view.getLbl_BitreteAnswer().setText((Integer.toString(grabber.getVideoBitrate() / 1024)) + " kb/s");
        Start.view.getLbl_FrameRateAnwer().setText(Integer.toString((int)Math.round(grabber.getFrameRate() * 100.0  / 100.0)) + " fps");
        check2("после вывода инфы о видео");
        status = true;

        jProgressBar = View.getJProgressBar();
        jLabel = View.getLabel1();
        int frameCount = grabber.getLengthInFrames();
        IplImage frame = converter.convert(grabber.grab());
        IplImage image = null;
        IplImage prevImage = null;
        IplImage diff = null;
        ArrayList<IplImage> arrayListDiff = new ArrayList<IplImage>();
        ArrayList<Float> arrayListSizPoint = new ArrayList<Float>();


//        ImageIcon icon1 = new ImageIcon("/home/alex/Java_projects/sbtjava/images/Rice.jpg");
//        //JLabel label = new JLabel(icon1);
//        //panel.add(label).setBounds(10,10,27,30);
//        View.getLabel1().setIcon(icon1);


//        CanvasFrame canvasFrame = new CanvasFrame("Current frame");
//        canvasFrame.setCanvasSize(frame.width(), frame.height());


        CvMemStorage storage = CvMemStorage.create();

//        while (canvasFrame.isVisible() && (frame = converter.convert(grabber.grab())) != null) {
        Frame base_frame = grabber.grab();

        jProgressBar.setMinimum(0);
        jProgressBar.setMaximum(frameCount - 1);
        jProgressBar.setValue(0);
        //check(pathToVideo);
        outer:
        while (base_frame != null) {

//            base_frame = grabber.grab();
            frame = converter.convert(base_frame);
            if (frame == null) {
                base_frame = grabber.grab();
                continue outer;
            }
            cvClearMemStorage(storage);

//            cvSmooth(frame, frame, CV_GAUSSIAN, 9, 9, 2, 2);
            if (image == null) {
                image = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
                cvCvtColor(frame, image, CV_RGB2GRAY);
            } else {
                prevImage = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
                prevImage = image;
                image = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
                cvCvtColor(frame, image, CV_RGB2GRAY); //Конвертирование изображения в другое цветовое пространство
            }

            if (diff == null) {
                diff = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
            }
            arrayListDiff.add(diff);
            float totalSize = 0.0f;
            if (prevImage != null) {
                // perform ABS difference
                cvAbsDiff(image, prevImage, diff);
                // do some threshold for wipe away useless details
                cvThreshold(diff, diff, 64, 255, CV_THRESH_BINARY);

                //canvasFrame.showImage(converter.convert(diff));

                // recognize contours
                CvSeq contour = new CvSeq(null);
                cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

                while (contour != null && !contour.isNull()) {
                    if (contour.elem_size() > 0) {
                        Integer term = contour.elem_size();
                        CvBox2D box = cvMinAreaRect2(contour, storage);
                        // test intersection
                        if (box != null) {
                            CvPoint2D32f center = box.center();
                            CvSize2D32f size = box.size();
                            float sizeXPoint = size.get(0);
                            float sizeYPoint = size.get(1);
                            totalSize = totalSize + sizeXPoint * sizeYPoint;
                            if(totalSize > 2500)
                                totalSize = 2500;
                            /*for (int i = 0; i < sa.length; i++) {
                                if ((Math.abs(center.x - (sa[i].offsetX + sa[i].width / 2))) < ((size.width / 2) + (sa[i].width / 2)) &&
                                    (Math.abs(center.y - (sa[i].offsetY + sa[i].height / 2))) < ((size.height / 2) + (sa[i].height / 2))) {

                                    if (!alarmedZones.containsKey(i)) {
                                        alarmedZones.put(i, true);
                                        activeAlarms.put(i, 1);
                                    } else {
                                        activeAlarms.remove(i);
                                        activeAlarms.put(i, 1);
                                    }
                                    System.out.println("Motion Detected in the area no: " + i +
                                            " Located at points: (" + sa[i].x + ", " + sa[i].y+ ") -"
                                            + " (" + (sa[i].x +sa[i].width) + ", "
                                            + (sa[i].y+sa[i].height) + ")");
                                }
                            }*/
                            String test = "asdf";
                        }
                    }
                    contour = contour.h_next();
                }
            }
            arrayListSizPoint.add(totalSize);

//            //show current frame in main frame
            ImageIcon icon1 = new ImageIcon(iplImageToBufferedImage(image));
            ImageIcon icon2 = new ImageIcon(icon1.getImage().getScaledInstance(344, 289, Image.SCALE_SMOOTH));
            View.getLabel1().setIcon(icon2);



            jProgressBar.setValue(arrayListSizPoint.size());
            Start.view.getLbl_ProcessedAnswer().setText(Integer.toString(arrayListSizPoint.size()) + " / " + Integer.toString(frameCount - 1));

            long startTime = System.currentTimeMillis();
            //Start.view.getLbl_TimeAnwer().setText(Long.toString(startTime));
//            cvWaitKey(30);
            base_frame = grabber.grab();
        }
        grabber.stop();
        //canvasFrame.dispose();

        //String path = "main_picture.png";
        URL imgURL = View.class.getResource(View.path);
        ImageIcon icon3 = new ImageIcon(imgURL);
        ImageIcon icon4 = new ImageIcon(icon3.getImage().getScaledInstance(344, 289, Image.SCALE_SMOOTH));
        View.getLabel1().setIcon(icon4);

        Controller.results = arrayListSizPoint;

        //status = false;

        JOptionPane.showMessageDialog(null, "Video processed! Press \"Export to Excell\" to get results");

        return arrayListSizPoint;
    }

    static OpenCVFrameConverter.ToIplImage grabberConverter = new OpenCVFrameConverter.ToIplImage();
    static Java2DFrameConverter paintConverter = new Java2DFrameConverter();
    public static BufferedImage iplImageToBufferedImage(IplImage src) {
        Frame frame = grabberConverter.convert(src);
        return paintConverter.getBufferedImage(frame,1);
    }
    private void check(File file){
        if(file.exists())
            JOptionPane.showMessageDialog(null, "есть видео");
        else
            JOptionPane.showMessageDialog(null, "нет видео");


    }

    private void check2(String path){
            JOptionPane.showMessageDialog(null, path);


    }
}
